@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
       <h2>Buat Account Baru</h2>
       <h3>Sign Up Form</h3>

        <form action="/welc0me" method="POST">
            @csrf
            <label>First Name:</label><br><br>
            <input type="text" name="Nama_Depan"><br><br>
            <label>Last Name:</label><br><br>
            <input type="text" name="Nama_Belakang"><br><br>
            <label>Gender :</label><br><br>
            <input type="radio">Male<br>
            <input type="radio">Female<br><br>

            <label>Nationality</label><br><br>
                <select name="nationality">
                    <option value="indonesia">Indonesia</option>
                    <option value="amerika">Amerika</option>
                    <option value="inggris">Inggris</option>
                </select> <br><br>

            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="language_spoken">Bahasa Indonesia<br>
            <input type="checkbox" name="language_spoken">English<br>
            <input type="checkbox" name="language_spoken">Other<br><br>

            <label>Bio</label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>

            <input type="submit" value="Sign Up"><br><br>
        </form>
@endsection