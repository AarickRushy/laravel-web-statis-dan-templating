<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('page.daftar');
    }

    public function welc0me(Request $request)
    {
        //dd($request->all());
        $Nama_Depan = $request["Nama_Depan"];
        $Nama_Belakang = $request["Nama_Belakang"];

        return view('page.welc0me', compact("Nama_Depan", "Nama_Belakang"));
    }
}
